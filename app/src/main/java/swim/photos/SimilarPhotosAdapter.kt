package swim.photos


import android.graphics.Bitmap
import android.graphics.drawable.Drawable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso
import com.squareup.picasso.Target
import java.lang.Exception


class SimilarPhotoAdapter(private val similarPhotosList: ArrayList<PhotoElement>) :
    RecyclerView.Adapter<SimilarPhotoAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view: View = LayoutInflater.from(parent.context).inflate(R.layout.single_similar, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount() = similarPhotosList.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val similarPhoto = similarPhotosList[position]
        Picasso.get()
            .load(similarPhoto.url)
            .into(object : Target {
                override fun onPrepareLoad(placeHolderDrawable: Drawable?) {
                    holder.photo.scaleType = ImageView.ScaleType.FIT_CENTER
                    holder.photo.setImageResource(R.drawable.think)
                }

                override fun onBitmapFailed(e: Exception?, errorDrawable: Drawable?) {
                    holder.photo.scaleType = ImageView.ScaleType.FIT_CENTER
                    holder.photo.setImageResource(R.drawable.nope)
                }

                override fun onBitmapLoaded(bitmap: Bitmap?, from: Picasso.LoadedFrom?) {
                    holder.photo.scaleType = ImageView.ScaleType.CENTER_CROP
                    holder.photo.setImageBitmap(bitmap)
                }
            })
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val photo: ImageView = itemView.findViewById(R.id.single_similar_photoIV)
    }
}