package swim.photos

import android.app.Activity
import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.gson.Gson
import kotlinx.android.synthetic.main.activity_main.*
import com.google.gson.reflect.TypeToken


class MainContent : AppCompatActivity() {

    private lateinit var recyclerView: RecyclerView
    private lateinit var viewAdapter: RecyclerView.Adapter<*>
    private lateinit var viewManager: RecyclerView.LayoutManager

    private lateinit var sharedPref: SharedPreferences
    private lateinit var photoAdapter: PhotoAdapter

//    val adapterPhoto: PhotoAdapter = recyclerView.photoAdapter as PhotoAdapter


    var data = mutableListOf<PhotoElement>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbar as Toolbar?)

        sharedPref = this.getSharedPreferences("photo", 0)
        data = getMutableList("photo")

//        photoAdapter = PhotoAdapter(data, this)

        viewManager = LinearLayoutManager(this)
        viewAdapter = PhotoAdapter(data, this)

        recyclerView = findViewById<RecyclerView>(R.id.content_main).apply {
            setHasFixedSize(true)
            layoutManager = viewManager
            adapter = viewAdapter
        }

        photoAdapter = recyclerView.adapter as PhotoAdapter

        val swipeHandler = object : SwipeToDeleteCallback(this) {
            override fun onMove(p0: RecyclerView, p1: RecyclerView.ViewHolder, p2: RecyclerView.ViewHolder): Boolean {
                return false
            }

            override fun onSwiped(p0: RecyclerView.ViewHolder, p1: Int) {
                photoAdapter.removeAt(p0.adapterPosition)
//                photoAdapter.notifyDataSetChanged()
//                photoAdapter.notifyItemRemoved(p0.layoutPosition)
            }
        }

        val itemTouchHelper = ItemTouchHelper(swipeHandler)
        itemTouchHelper.attachToRecyclerView(recyclerView)
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.menu_main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        when (item.itemId) {
            R.id.add_photo -> {
                val intent = Intent(this, AddPhotoActivity::class.java)
                startActivityForResult(intent, 758)
            }
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (resultCode == Activity.RESULT_OK) {
            val photoString = data!!.getStringExtra("photo")
//            this.data.add(Gson().fromJson<PhotoElement>(photoString, PhotoElement::class.java))
            this.data.apply { photoAdapter.addItem(Gson().fromJson<PhotoElement>(photoString, PhotoElement::class.java)) }
            viewAdapter.notifyDataSetChanged()
        }
    }

    private fun getMutableList(key: String): MutableList<PhotoElement> {
        class Token : TypeToken<MutableList<PhotoElement>>()
        val keyList = sharedPref.getString(key, null)
        if (keyList != null) {
            return Gson().fromJson(keyList, Token().type)
        }
        return mutableListOf()
    }


}