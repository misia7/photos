package swim.photos

import android.graphics.Bitmap
import android.graphics.drawable.Drawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.fragment.app.Fragment
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.squareup.picasso.Picasso
import com.squareup.picasso.Target
import kotlinx.android.synthetic.main.fragment_fullscreen_photo.*

class FullscreenPhotoFragment : Fragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_fullscreen_photo, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val photoKey = arguments?.getString(PHOTO)!!

        val photo: PhotoElement = Gson().fromJson(photoKey, PhotoElement::class.java)

        Picasso.get().load(photo.url).into(object : Target {
            override fun onPrepareLoad(placeHolderDrawable: Drawable?) {
                fullscreen_imageview.scaleType = ImageView.ScaleType.FIT_CENTER
                fullscreen_imageview.setImageResource(R.drawable.think)
            }

            override fun onBitmapFailed(e: Exception?, errorDrawable: Drawable?) {
                fullscreen_imageview.scaleType = ImageView.ScaleType.FIT_CENTER
                fullscreen_imageview.setImageResource(R.drawable.nope)
            }

            override fun onBitmapLoaded(bitmap: Bitmap?, from: Picasso.LoadedFrom?) {
                fullscreen_imageview.scaleType = ImageView.ScaleType.CENTER_CROP
                fullscreen_imageview.setImageBitmap(bitmap)
            }

        })

    }

    companion object {
        private const val PHOTO = "one"

        @JvmStatic
        fun newInstance(photo: PhotoElement) =
            FullscreenPhotoFragment().apply {
                arguments = Bundle().apply {
                    putString(PHOTO, Gson().toJson(photo))
                }
            }
    }
}
