package swim.photos

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import kotlinx.android.synthetic.main.fragment_photo_info.*


class PhotoInfo : Fragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_photo_info, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {

        class Token : TypeToken<PhotoElement>()
        val photoKey = arguments?.getString(PHOTO)!!
        val photo: PhotoElement = Gson().fromJson(photoKey, Token().type)
        details_title.text = photo.title
        details_tags.text = photo.tags.take(3).joinToString(" #", prefix = "#")
        details_date.text = photo.date

    }

    companion object {
        private const val PHOTO = "photo_info"
        @JvmStatic
        fun newInstance(photo: PhotoElement) =
            PhotoInfo().apply {
                arguments = Bundle().apply {
                    putString(PHOTO, Gson().toJson(photo))
                }
            }
    }
}
