package swim.photos

import android.content.Intent
import android.content.Context
import android.graphics.Bitmap
import android.graphics.drawable.Drawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.core.content.ContextCompat.startActivity
import androidx.recyclerview.widget.RecyclerView
import com.google.firebase.ml.vision.FirebaseVision
import com.google.firebase.ml.vision.common.FirebaseVisionImage
import com.google.gson.Gson
import com.squareup.picasso.Picasso
import com.squareup.picasso.Target
import java.lang.Exception

class PhotoAdapter(private val data: MutableList<PhotoElement>, private val context: Context) :
    RecyclerView.Adapter<PhotoAdapter.ViewHolder>() {

    private val sharedPref = context.getSharedPreferences("photo", 0)

    class ViewHolder(View: View) : RecyclerView.ViewHolder(View) {
        val title: TextView = itemView.findViewById(R.id.title_text)
        val tags: TextView = itemView.findViewById(R.id.tags_text)
        val date: TextView = itemView.findViewById(R.id.date_text)
        val photo: ImageView = itemView.findViewById(R.id.photo)


        fun updateWithUrl(elem:PhotoElement) {
            Picasso.get()
                .load(elem.url)
                .into(object : Target {
                    override fun onPrepareLoad(placeHolderDrawable: Drawable?) {
                        tags.text = "Loading..."
                        photo.scaleType = ImageView.ScaleType.FIT_CENTER
                        photo.setImageResource(R.drawable.think)
                    }

                    override fun onBitmapFailed(e: Exception?, errorDrawable: Drawable?) {
                        tags.text = "Failure :("
                        photo.scaleType = ImageView.ScaleType.FIT_CENTER
                        photo.setImageResource(R.drawable.nope)
                    }

                    override fun onBitmapLoaded(bitmap: Bitmap?, from: Picasso.LoadedFrom?) {
                        displayTags(bitmap!!, elem)
                        photo.scaleType = ImageView.ScaleType.CENTER_CROP
                        photo.setImageBitmap(bitmap)
                    }
                }
                )

        }


        private fun displayTags(bitmap: Bitmap, elem: PhotoElement) {
            val whatever = FirebaseVisionImage.fromBitmap(bitmap)
            val labeler = FirebaseVision.getInstance().onDeviceImageLabeler

            labeler.processImage(whatever)
                .addOnSuccessListener { labels ->
                    this.tags.text = labels.joinToString(" #", prefix = "#", limit = 3) { it.text }
                    elem.tags.addAll(labels.map { it.text })
                }
        }
    }


    private fun getSimilarPhotos(photo: PhotoElement): List<PhotoElement> {
        val photos = data.filter { item ->
            item.tags.filter { tag -> photo.tags.contains(tag) }.isNotEmpty() && item != photo
        }.sortedByDescending { item ->
            item.tags.filter { tag -> photo.tags.contains(tag) }.size
        }
        return photos.take(6)

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val View = LayoutInflater.from(parent.context).inflate(R.layout.photo_element_layout, parent, false)
        return ViewHolder(View)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val elem: PhotoElement = data[position]

        holder.title.text = elem.title
        holder.tags.text = elem.getTags(3)
        holder.date.text = elem.date
        holder.updateWithUrl(elem)

        holder.photo.setOnClickListener {
            val context = holder.photo.context
            val intent = Intent(context, PhotoActivity::class.java)
            intent.putExtra(PhotoActivity.PHOTO_KEY, Gson().toJson(elem))
            val similarPhotos: ArrayList<PhotoElement>
            val photos = getSimilarPhotos(elem)
            similarPhotos = when {
                photos.isEmpty() -> arrayListOf()
                photos.size == 1 -> ArrayList(photos)
                else -> photos as ArrayList<PhotoElement>
            }
            intent.putExtra(PhotoActivity.SIMILAR_PHOTOS, Gson().toJson(similarPhotos))
            startActivity(context, intent, Bundle.EMPTY)
        }

    }

    override fun getItemCount() = data.size

    fun removeAt(position: Int) {
        data.removeAt(position)
        notifyItemRemoved(position)
        saveToSharedPref("photo", data)
    }

    fun addItem(photo: PhotoElement) {
        data.add(0, photo)
        notifyItemInserted(0)
        saveToSharedPref("photo", data)
    }

    private fun saveToSharedPref(keyName: String, list: MutableList<PhotoElement>) {
        val listJson = Gson().toJson(list)
        val editor = sharedPref.edit()
        editor.putString(keyName, listJson)
        editor!!.apply()
    }
}