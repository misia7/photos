package swim.photos

class PhotoElement (val url : String, val title : String, val date : String, val tags : MutableList<String> = mutableListOf()) {

    fun getTags(num : Int): String {
        var result = ""
        var number = num
        if(tags.size < num)
            number = tags.size
        for (i in 0..number)
            result += " $tags[i]"
        return result
    }
}