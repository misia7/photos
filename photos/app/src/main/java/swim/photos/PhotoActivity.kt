package swim.photos

import android.os.Bundle
//import android.support.design.widget.TabLayout
import androidx.appcompat.app.AppCompatActivity
import androidx.viewpager.widget.ViewPager
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import kotlinx.android.synthetic.main.activity_main.*

//import android.support.v4.view.ViewPager

class PhotoActivity : AppCompatActivity() {

    companion object {
        const val PHOTO_KEY = "key"
        const val SIMILAR_PHOTOS = "similar"
    }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_photo)
        setSupportActionBar(toolbar)

        class Token : TypeToken<PhotoElement>()
        val photoKey = intent.getStringExtra(PHOTO_KEY)
        val photo: PhotoElement = Gson().fromJson(photoKey, Token().type)

        class Token2 : TypeToken<ArrayList<PhotoElement>>()
        val similarPhotosKey = intent.getStringExtra(SIMILAR_PHOTOS)
        val similarPhotos: ArrayList<PhotoElement> = Gson().fromJson(similarPhotosKey, Token2().type)

        val viewPager: ViewPager = findViewById(R.id.view_pager)
        viewPager.adapter = SectionsPagerAdapter(this, supportFragmentManager, photo, similarPhotos )
//        val tabs: TabLayout = findViewById(R.id.tabs)
//        tabs.setupWithViewPager(viewPager)
        //To work with androidx needs TabLayoutMediator
    }
}
