package swim.photos

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.GridLayoutManager
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import kotlinx.android.synthetic.main.fragment_similar_photos.*

class SimilarPhotos : Fragment() {
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_similar_photos, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        class Token : TypeToken<ArrayList<PhotoElement>>()
        val photosKey = arguments?.getString(SIMILAR_PHOTOS)!!
        val similarPhotos: ArrayList<PhotoElement> = Gson().fromJson(photosKey, Token().type)

        similar_photosRV.layoutManager = GridLayoutManager(activity, 2)
        similar_photosRV.adapter = SimilarPhotoAdapter(similarPhotos)
    }

    companion object {
        private const val SIMILAR_PHOTOS = "similar_photos"

        @JvmStatic
        fun newInstance(similarPhotos: ArrayList<PhotoElement>) = SimilarPhotos().apply {
            arguments = Bundle().apply {
                putString(SIMILAR_PHOTOS, Gson().toJson(similarPhotos))
            }
        }
    }
}
