package swim.photos

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken

class DividedFragment : Fragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_divided, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        class Token : TypeToken<ArrayList<PhotoElement>>()
        val photosKey = arguments?.getString(SIMILAR_PHOTOS)!!
        val similarPhotos: ArrayList<PhotoElement> = Gson().fromJson(photosKey, Token().type)

        class Token2 : TypeToken<PhotoElement>()
        val photoKey = arguments?.getString(PHOTO_ITEM)!!
        val photo: PhotoElement = Gson().fromJson(photoKey, Token2().type)

        childFragmentManager.beginTransaction().apply {
            replace(R.id.photoInfo, PhotoInfo.newInstance(photo))
            replace(R.id.similarPhotos, SimilarPhotos.newInstance(similarPhotos))
            commit()
        }
    }

    companion object {
        private const val PHOTO_ITEM = "photo_item"
        private const val SIMILAR_PHOTOS = "similar_photos"

        @JvmStatic
        fun newInstance(photo: PhotoElement, similarPhotos: ArrayList<PhotoElement>) =
            DividedFragment().apply {
                arguments = Bundle().apply {
                    putString(PHOTO_ITEM, Gson().toJson(photo))
                    putString(SIMILAR_PHOTOS, Gson().toJson(similarPhotos))
                }
            }
    }
}
