package swim.photos

import android.content.Context
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter

private val TAB_TITLES = arrayOf(
    R.string.tab_text_1,
    R.string.tab_text_2
)

class SectionsPagerAdapter(
    private val context: Context,
    fm: FragmentManager?,
    private val photo: PhotoElement,
    private val similarPhotos: ArrayList<PhotoElement>
) : FragmentPagerAdapter(fm) {

    override fun getItem(position: Int): Fragment {
        return when (position) {
            0 -> FullscreenPhotoFragment.newInstance(photo)
            1 -> DividedFragment.newInstance(photo, similarPhotos)
            else -> {
                FullscreenPhotoFragment.newInstance(photo)
            }
        }
    }

    override fun getPageTitle(position: Int): CharSequence? {
        return context.resources.getString(TAB_TITLES[position])
    }

    override fun getCount(): Int {
        return 2
    }
}