package swim.photos

import android.app.Activity
import android.app.DatePickerDialog
import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.google.gson.Gson
import kotlinx.android.synthetic.main.add_photo_layout.*
import java.text.SimpleDateFormat
import java.util.*

class AddPhotoActivity : AppCompatActivity() {

    private val calendar = Calendar.getInstance()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.add_photo_layout)
//        setSupportActionBar(toolbar)

        add_button.setOnClickListener {
            val url = url_input.text.toString()
            val title = title_input.text.toString()
            val date = date_input.text.toString()

            if (url != "") {
                var elem = PhotoElement(url, title, date)

                val intent = Intent()
                intent.putExtra("photo", Gson().toJson(elem))
                setResult(Activity.RESULT_OK, intent)
                finish()
            }
        }

        date_input.setOnClickListener {
            DatePickerDialog(
                this, dateSetListener,
                calendar.get(Calendar.YEAR),
                calendar.get(Calendar.MONTH),
                calendar.get(Calendar.DAY_OF_MONTH)
            ).show()
        }
    }

    private val dateSetListener = DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->
        calendar.set(Calendar.YEAR, year)
        calendar.set(Calendar.MONTH, monthOfYear)
        calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth)

        val df = SimpleDateFormat("kk:mm:ss   dd.MM.yyyy", Locale.getDefault())
        date_input.setText(df.format(calendar.time))
    }

}